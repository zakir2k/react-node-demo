/** @jsx React.DOM */

var React = require('react');
var TweetsApp = require('./components/TweetsApp.react');

// Initial state passed from the server side
var initialState = JSON.parse(document.getElementById('initial-state').innerHTML);

// Render the components -> using initialState.
React.renderComponent(
  <TweetsApp tweets={initialState}/>,
  document.getElementById('react-app')
);